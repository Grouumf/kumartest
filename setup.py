from setuptools import setup, find_packages
import sys, os

VERSION = '0.0.1'

setup(name='kumartest',
      version=VERSION,
      description="kumar_test",
      long_description="""""",
      classifiers=[],
      keywords='bullshit',
      author='o_poirion',
      author_email='o.poirion@gmail.com',
      url='',
      license='MIT',
      packages=find_packages(exclude=['examples', 'tests']),
      include_package_data=True,
      zip_safe=False,
      install_requires=[],
      )
